# You can set the default NSO_IMAGE_PATH & PKG_PATH to point to your docker
# registry so that developers don't have to manually set these variables.
# Similarly for NSO_VERSION you can set a default version. Note how the ?=
# operator only sets these variables if not already set, thus you can easily
# override them by explicitly setting them in your environment and they will be
# overridden by variables in CI.
# Default variables:
#export NSO_IMAGE_PATH ?= registry.example.com:5000/my-group/nso-docker/
#export PKG_PATH ?= registry.example.com:5000/my-group/
export NSO_VERSION ?= 5.4

# Include standard NID (NSO in Docker) package Makefile that defines all
# standard make targets
include nidpackage.mk

# The rest of this file is specific to this repository.

# TODO fix for IPv6 too?
CONTAINER_IP_GRAFANA=$(shell docker inspect $(CNT_PREFIX)-grafana | jq -r '.[].NetworkSettings.Networks | .[].IPAddress')
CONTAINER_IP_JAEGER=$(shell docker inspect $(CNT_PREFIX)-jaeger | jq -r '.[].NetworkSettings.Networks | .[].IPAddress')
CONTAINER_IP_KIBANA=$(shell docker inspect $(CNT_PREFIX)-kibana | jq -r '.[].NetworkSettings.Networks | .[].IPAddress')
CONTAINER_IP_GRAFANA=$(shell docker inspect $(CNT_PREFIX)-grafana | jq -r '.[].NetworkSettings.Networks | .[].IPAddress')
CONTAINER_IP_INFLUXDB=$(shell docker inspect $(CNT_PREFIX)-influxdb | jq -r '.[].NetworkSettings.Networks | .[].IPAddress')


testenv-start-extra:
	@echo "\n== Starting repository specific testenv"
	#docker run -td --name $(CNT_PREFIX)-dev1 --network-alias dev1 $(DOCKER_ARGS) $(PKG_PATH)ned-ietf-yang/netsim:$(NSO_VERSION)
	@echo "-- Starting elasticsearch"
	docker run -td --name $(CNT_PREFIX)-elasticsearch --network-alias elasticsearch $(DOCKER_ARGS) \
		-e "discovery.type=single-node" elasticsearch:7.8.0
	@echo "-- Starting Kibana"
	docker run -td --name $(CNT_PREFIX)-kibana --network-alias kibana $(DOCKER_ARGS) kibana:7.8.0
	@echo "-- Starting Grafana"
	docker run -td --name $(CNT_PREFIX)-grafana --network-alias grafana $(DOCKER_ARGS) \
    -e GF_AUTH_ANONYMOUS_ENABLED=true \
    -e GF_AUTH_ANONYMOUS_ORG_NAME=NSO \
    -e GF_AUTH_ANONYMOUS_ORG_ROLE=Admin \
    -e GF_AUTH_DISABLE_LOGIN_FORM=true \
    -e GF_AUTH_DISABLE_SIGNOUT_MENU=true \
    -e GF_DASHBOARDS_JSON_ENABLED=true \
		grafana/grafana
	@echo "-- Starting Jaeger collector"
	docker run -td --name $(CNT_PREFIX)-jaeger-collector --network-alias jaeger-collector $(DOCKER_ARGS) \
		-e SPAN_STORAGE_TYPE=elasticsearch -e ES_SERVER_URLS=http://elasticsearch:9200/ --restart=always jaegertracing/jaeger-collector:1.18.1
	@echo "-- Starting Jaeger agent"
	docker run -td --name $(CNT_PREFIX)-jaeger-agent --network=container:$(CNT_PREFIX)-nso $(DOCKER_LABEL_ARG) \
	  jaegertracing/jaeger-agent:1.18.1 --reporter.grpc.host-port=jaeger-collector:14250
	@echo "-- Starting Jaeger query"
	docker run -td --name $(CNT_PREFIX)-jaeger --network-alias jaeger $(DOCKER_ARGS) -p 16686:16686 -p 16687:16687 \
		-e SPAN_STORAGE_TYPE=elasticsearch -e ES_SERVER_URLS=http://elasticsearch:9200/ --restart=always jaegertracing/jaeger-query:1.18.1
	docker run -td --name $(CNT_PREFIX)-node-exporter --network-alias node-exporter $(DOCKER_ARGS) prom/node-exporter
	@echo "-- Starting InfluxDB"
	docker run -td --name $(CNT_PREFIX)-influxdb --network-alias influxdb $(DOCKER_ARGS) influxdb
	@echo "-- Wait for NSO to start up"
	docker exec -t $(CNT_PREFIX)-nso bash -lc 'ncs --wait-started 600'
	@echo "-- Configuring opentelemetry export of data in NSO"
	$(MAKE) testenv-runcmdJ CMD="unhide debug\nconfigure\nedit progress export\n set enabled\ncommit"
	$(MAKE) testenv-runcmdJ CMD="unhide debug\nconfigure\nedit progress trace ptrace\n set enabled verbosity very-verbose destination file ptrace.csv format csv\ncommit"
	$(MAKE) testenv-runcmdJ CMD="configure\nset python-vm logging vm-levels opentelemetry-exporter level level-debug\ncommit"
	$(MAKE) testenv-prepare
	$(MAKE) testenv-print-ui-addresses


testenv-prepare-grafana:
	@echo "-- Configure Grafana org name"
	curl "http://admin:admin@$(CONTAINER_IP_GRAFANA):3000/api/orgs/1" \
		-m 5 -X PUT \
		-H 'Content-Type: application/json;charset=UTF-8' \
		--data-binary "{\"name\":\"NSO\"}"; echo
	@echo "-- Add Grafana data source for ElasticSearch"
	curl "http://admin:admin@$(CONTAINER_IP_GRAFANA):3000/api/datasources" \
			-m 5 -X POST \
			-H 'Content-Type: application/json;charset=UTF-8' \
			--data-binary '{"id":1,"orgId":1,"name":"Elasticsearch","type":"elasticsearch","typeLogoUrl":"public/app/plugins/datasource/elasticsearch/img/elasticsearch.svg","access":"proxy","url":"http://elasticsearch:9200","password":"","user":"","database":"","basicAuth":false,"isDefault":false,"jsonData":{"esVersion":70,"logLevelField":"","logMessageField":"","maxConcurrentShardRequests":5,"timeField":"@timestamp"},"readOnly":false}'; echo
	@echo "-- Add Grafana data source for InfluxDB"
	curl "http://admin:admin@$(CONTAINER_IP_GRAFANA):3000/api/datasources" \
			-m 5 -X POST \
			-H 'Content-Type: application/json;charset=UTF-8' \
			--data-binary '{"id":2,"orgId":1,"name":"InfluxDB","type":"influxdb","typeLogoUrl":"public/app/plugins/datasource/influxdb/img/influxdb_logo.svg","access":"proxy","url":"http://influxdb:8086","password":"","user":"","database":"nso","basicAuth":false,"isDefault":false,"jsonData":{"esVersion":70,"logLevelField":"","logMessageField":"","maxConcurrentShardRequests":5,"timeField":"@timestamp"},"readOnly":false}'; echo
	@echo "-- Add Grafana dashboard for node-exporter"
	@curl "http://admin:admin@$(CONTAINER_IP_GRAFANA):3000/api/dashboards/db/node" \
			-m 5 -X DELETE \
			-H 'Content-Type: application/json;charset=UTF-8'; echo
	@curl "http://admin:admin@$(CONTAINER_IP_GRAFANA):3000/api/dashboards/db" \
			-m 5 -X POST \
			-H 'Content-Type: application/json;charset=UTF-8' \
			--data-binary @dashboard-node.json; echo
	@echo "-- Add Grafana dashboard for NSO (replace any old by deleting it first)"
	@curl "http://admin:admin@$(CONTAINER_IP_GRAFANA):3000/api/dashboards/db/nso" \
			-m 5 -X DELETE \
			-H 'Content-Type: application/json;charset=UTF-8'; echo
	@curl "http://admin:admin@$(CONTAINER_IP_GRAFANA):3000/api/dashboards/db" \
			-m 5 -X POST \
			-H 'Content-Type: application/json;charset=UTF-8' \
			--data-binary @dashboard-nso.json; echo
	@echo "-- Set NSO dashboard as default dashboard"
	@curl 'http://admin:admin@$(CONTAINER_IP_GRAFANA):3000/api/org/preferences' \
			-m 5 -X PUT \
			-H 'X-Grafana-Org-Id: 1' \
			-H 'Content-Type: application/json;charset=UTF-8' \
			--data-binary "{\"homeDashboardId\":$$(curl -m 5 'http://admin:admin@$(CONTAINER_IP_GRAFANA):3000/api/dashboards/uid/nso' 2>/dev/null | jq .dashboard.id)}"; echo


testenv-prepare:
	@echo "\n== Prepare testenv"
	#@echo "-- Add device to NSO"
	#@echo "   Get the package-meta-data.xml file from the compiled NED (we grab it from the netsim build)"
	#mkdir -p tmp
	#docker cp $(CNT_PREFIX)-dev1:/var/opt/ncs/packages/ned-ietf-yang/package-meta-data.xml tmp/package-meta-data.xml
	#@echo "   Fill in the device-type in add-device.xml by extracting the relevant part from the package-meta-data of the NED"
	#echo $(NSO_VERSION) | grep "^4" && xmlstarlet sel -N x=http://tail-f.com/ns/ncs-packages -t -c "//x:ned-id" tmp/package-meta-data.xml | grep cli && STRIP_NED=' -d "//x:ned-id" '; \
	#	xmlstarlet sel -R -N x=http://tail-f.com/ns/ncs-packages -t -c "//*[x:ned-id]" -c "document('test/add-device.xml')" tmp/package-meta-data.xml | xmlstarlet edit -O -N x=http://tail-f.com/ns/ncs-packages -N y=http://tail-f.com/ns/ncs -d "/x:xsl-select/*[x:ned-id]/*[not(self::x:ned-id)]" -m "/x:xsl-select/*[x:ned-id]" "/x:xsl-select/y:devices/y:device/y:device-type" $${STRIP_NED} | tail -n +2 | sed '$$d' | cut -c 3- > tmp/add-device.xml
	#docker cp tmp/add-device.xml $(CNT_PREFIX)-nso:/add-device.xml
	#$(MAKE) testenv-runcmdJ CMD="configure\nload merge /add-device.xml\ncommit\nexit"
	#$(MAKE) testenv-runcmdJ CMD="show devices brief"
	sleep 20
	$(MAKE) testenv-runcmdJ CMD="unhide debug\nconfigure\nedit progress export\n set enabled\nset jaeger\nset influxdb host influxdb\nset jaeger-base-url http://$(CONTAINER_IP_JAEGER):16686\ncommit\nexit\nexit\nrequest packages reload"
	$(MAKE) testenv-prepare-grafana


testenv-print-ui-addresses:
	@echo "Visit the following URLs in your web browser to reach respective system:"
	@echo "Jaeger    : http://$(CONTAINER_IP_JAEGER):16686"
	@echo "Kibana    : http://$(CONTAINER_IP_KIBANA):5601"
	@echo "Grafana   : http://$(CONTAINER_IP_GRAFANA):3000"
	@echo "InfluxDB  : http://$(CONTAINER_IP_INFLUXDB):8086"


testenv-test:
	@echo "\n== Running tests"
	$(MAKE) testenv-test-concurrent

testenv-test-simple:
	$(MAKE) testenv-runcmdJ CMD="configure\ndelete slow-service\ncommit"
	$(MAKE) testenv-runcmdJ CMD="configure\nset slow-service FOO random-create-slowness 0.5\ncommit"

testenv-test-simple-slow:
	$(MAKE) testenv-runcmdJ CMD="configure\ndelete slow-service\ncommit"
	$(MAKE) testenv-runcmdJ CMD="configure\nset slow-service FOO create-slowness 2\ncommit"

testenv-test-simple-continuous:
	while true; do make testenv-test-simple; sleep $$(awk -v min=0 -v max=5 'BEGIN{srand(); print int(min+rand()*(max-min+1))}'); done

testenv-test-simple-redeploy:
	$(MAKE) testenv-runcmdJ CMD="request slow-service FOO re-deploy"

testenv-test-simple-dry-run:
	$(MAKE) testenv-runcmdJ CMD="configure\nset slow-service DRY\ncommit dry-run"

testenv-test-concurrent-slowAF:
	$(MAKE) testenv-runcmdJ CMD="configure\ndelete slow-service\ncommit"
	$(MAKE) -j20 testenv-test-concurrent-run CSLOW=60

TEST_INSTANCES=A B C D E F G H I J K L M N O P Q R S T U V W X Y Z AA AB AC AD AE AF AG AH AI AJ AK AL AM AN AO AP AQ AR AS AT AU AV AW AX AY AZ

testenv-test-concurrent:
	$(MAKE) testenv-runcmdJ CMD="configure\ndelete slow-service\ncommit"
	$(MAKE) -j10 testenv-test-concurrent-run CSLOW=2

testenv-test-concurrent-run: $(addsuffix testenv-test-concurrent,$(TEST_INSTANCES))

$(addsuffix testenv-test-concurrent,$(TEST_INSTANCES)):
	$(MAKE) testenv-runcmdJ CMD="configure\nset slow-service $(subst testenv-test-concurrent,,$@) create-slowness $(CSLOW)\ncommit"

testenv-test-service-trace:
	$(MAKE) testenv-runcmdJ CMD="configure\ndelete slow-service\ncommit"
	$(MAKE) testenv-runcmdJ CMD="configure\nset slow-service S1\nset slow-service S2\ncommit"
	$(MAKE) testenv-runcmdJ CMD="configure\nset slow-service S1 random-create-slowness 0.1\ncommit"
	$(MAKE) testenv-runcmdJ CMD="configure\nset slow-service S2 random-create-slowness 0.1\ncommit"
	$(MAKE) testenv-runcmdJ CMD="configure\nset slow-service S1 random-create-slowness 0.2\ncommit"

testenv-test-csv54:
	@echo "-- Disable opentelemetry output plugin"
	$(MAKE) testenv-runcmdJ CMD="unhide debug\nconfigure\nedit progress opentelemetry\n set disabled\ncommit"
	@echo "-- Temporarily disable CSV output and remove file to ensure fresh start"
	$(MAKE) testenv-runcmdJ CMD="unhide debug\nconfigure\ndelete progress trace ptrace\ncommit"
	sleep 1
	docker exec -t $(CNT_PREFIX)-nso bash -lc 'rm /log/ptrace.csv'
	@echo "-- Enabling CSV output"
	$(MAKE) testenv-runcmdJ CMD="unhide debug\nconfigure\nedit progress trace ptrace\n set enabled verbosity very-verbose destination file ptrace.csv format csv\ncommit"
	@echo "-- Running test"
	$(MAKE) testenv-test-simple
	@echo "-- Letting file output settle"
	sleep 3
	@echo "-- Parse and export to jaeger"
	docker exec -t $(CNT_PREFIX)-nso bash -lc 'export PYTHONPATH=/var/opt/ncs/packages/opentelemetry-exporter/pyvenv/lib/python3.7/site-packages/; python3 /var/opt/ncs/packages/opentelemetry-exporter/python/opentelemetry_exporter/ptrace.py --debug --csv /log/ptrace.csv'
	@echo "-- Re-enable opentelemetry output plugin"
	$(MAKE) testenv-runcmdJ CMD="unhide debug\nconfigure\nedit progress opentelemetry\n set enabled\ncommit"


# Device related tests
testenv-test-dev:
	$(MAKE) testenv-test-dev-ssh-fetch-host-keys
	$(MAKE) testenv-test-dev-sync-from

testenv-test-dev-ssh-fetch-host-keys:
	$(MAKE) testenv-runcmdJ CMD="request devices device dev1 ssh fetch-host-keys"

testenv-test-dev-sync-from:
	$(MAKE) testenv-runcmdJ CMD="request devices device dev1 sync-from"

testenv-test-dev-connect:
	$(MAKE) testenv-runcmdJ CMD="request devices device dev1 connect"

#!/usr/bin/env python3
"""ptrace export test suite

Multiple types of testing is done here. Most of it is based on reading and
producing CSV files which are then compared to expected CSV files.

We prefer this over a basic diff since we get a nicer diff output. It is
inefficient though as we read in entire CSV files in memory. As long as we use
small test files, this is fine.

We also test parts of the Python code by instantiating it here directly.
"""

import csv
import datetime
import time
import unittest

from opentelemetry import metrics, trace
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import SimpleExportSpanProcessor
from opentelemetry.sdk.trace.export.in_memory_span_exporter import (
    InMemorySpanExporter
)

import ptrace

def readcsv(filename):
    trace_ids = {}
    span_ids = {}
    with open(filename) as csvfile:
        csvreader = csv.DictReader(csvfile)
        res = []
        for row in csvreader:
            if 'PARENT SPAN' in row:
                # replace parent span with the index of the parent
                for idx in range(len(res)):
                    if res[idx]['SPAN ID'] == row['PARENT SPAN']:
                        row['PARENT SPAN'] = idx
            res.append(row)
    # replace trace-id & span-id with its index (based on when we first saw it)
    for row in res:
        trace_ids[row.get('TRACE ID', '')] = None
        trace_idx = list(trace_ids.keys()).index(row.get('TRACE ID', ''))
        row['TRACE ID'] = trace_idx
        span_ids[row.get('SPAN ID', '')] = None
        span_idx = list(span_ids.keys()).index(row.get('SPAN ID', ''))
        row['SPAN ID'] = span_idx
    return res

def comp(name):
    return readcsv(f"expected/{name}.csv"), readcsv(f"output/{name}.csv")


class TestUnits(unittest.TestCase):
    def setUp(self):
        self.maxDiff = None

    def test_fix_nano_events(self):
        events = [
            {"msg": "saving FASTMAP nano reverse diff-set and applying changes component {ncs:self self}: state ncs:init"},
            {"msg": "component {ncs:self self}: state ncs:init status change not-reached -> reached"},
            {"msg": "component {ncs:self self}: state ncs:ready precondition evaluated to false"},
            {"msg": "component {ncs:self self}: state abc:dhcp"},
            {"msg": "executing side effect for /abc:foobi-devices/device{flonk_openstack_vim2}/plan/component{abc:vnf-subscriptions vnf-subscriptions}/state{abc:configure} op create"},
            {"msg": "executing side effect for /abc:foobi-devices/device{flonk_openstack_vim2}/plan/component{abc:vnf-subscriptions vnf-subscriptions}/state{abc:configure} op delete"},
        ]
        from pprint import pprint
        res = list(ptrace.fix_nano_events(events))
        expected = [
            {"msg": "saving FASTMAP nano reverse diff-set and applying changes", "component": "ncs:self self", "state": "ncs:init"},
            {"msg": "component state status change not-reached -> reached", "component": "ncs:self self", "state": "ncs:init"},
            {"msg": "component state precondition evaluated to false", "component": "ncs:self self", "state": "ncs:ready"},
            {"msg": "component state", "component": "ncs:self self", "state": "abc:dhcp"},
            {"msg": "executing side effect on create", "component": "abc:vnf-subscriptions vnf-subscriptions", "state": "abc:configure"},
            {"msg": "executing side effect on delete", "component": "abc:vnf-subscriptions vnf-subscriptions", "state": "abc:configure"},
        ]
        for idx, line in enumerate(res):
            self.assertEqual(line, expected[idx])

    def test_fix_check_conflicts(self):
        events = [
            {"msg": "check conflicts (with transaction 1234)"}
        ]
        res = list(ptrace.fix_check_conflicts(events))
        expected = [
            {"msg": "check conflicts", "other_tid": '1234'}
        ]
        self.assertEqual(res, expected)


class TestCSV(unittest.TestCase):
    def setUp(self):
        self.maxDiff = None

    def test_simple53(self):
        self.assertEqual(*comp("simple53"))

    def test_simple54(self):
        #self.assertEqual(*comp("simple54"))
        exp, out = comp("simple54")
        # explicitly check that our transaction span was added
        self.assertEqual("transaction", out[0]['MESSAGE'])
        self.assertEqual("transaction", out[-1]['MESSAGE'])
        # check parent is correct for a few spans
        self.assertEqual(0, out[1]['PARENT SPAN'])
        self.assertEqual(1, out[2]['PARENT SPAN'])

        # make sure validate is the parent of the holding transaction lock span
        self.assertEqual('validate', out[2]['MESSAGE'])
        self.assertEqual('holding transaction lock', out[5]['MESSAGE'])
        self.assertEqual(2, out[5]['PARENT SPAN'])
        # make sure validate is the parent of the 'creating rollback file'
        # span, or inversely, that it is NOT the 'holding transaction lock'
        # that is parent
        self.assertEqual(2, out[6]['PARENT SPAN'])

        # ensure info event is associated with correct span and inherits
        # span-id & parent from its span
        self.assertEqual('start', out[21]['EVENT TYPE'])
        self.assertEqual('write-start', out[21]['MESSAGE'])
        self.assertEqual('info', out[22]['EVENT TYPE'])
        self.assertEqual('write-start', out[22]['MESSAGE'])
        self.assertEqual(out[21]['SPAN ID'], out[22]['SPAN ID'])
        self.assertEqual(out[21]['PARENT SPAN'], out[22]['PARENT SPAN'])

    def test_simple_device54(self):
        self.assertEqual(*comp("simple-device54"))

    def test_classic_service54(self):
        self.assertEqual(*comp("classic-service54"))

    def test_sync_from54(self):
        self.assertEqual(*comp("sync-from54"))
        out = readcsv("output/sync-from54.csv")
        # verify that the applying transaction span is a child of the
        # 'sync-from' span
        self.assertEqual('sync-from', out[0]['MESSAGE'])
        self.assertEqual('applying transaction', out[9]['MESSAGE'])
        self.assertEqual(0, out[9]['PARENT SPAN'])

    def test_ooo_simple54(self):
        self.assertEqual(*comp("ooo-simple54"))

    def test_no_tid54(self):
        self.assertEqual(*comp("no-tid54"))
        out = readcsv("output/no-tid54.csv")
        self.assertNotEqual(out[0]['TRACE ID'], out[1]['TRACE ID'])

    def test_restconf_ha_trans54(self):
        out = readcsv("output/restconf-ha-trans54.csv")

        # ensure info event is associated with correct span and inherits
        # span-id & parent from its span
        self.assertEqual("start", out[27]['EVENT TYPE'])
        self.assertEqual("prepare", out[27]['MESSAGE'])
        self.assertEqual("received prepare from all (available) slaves", out[30]['MESSAGE'])
        self.assertEqual(out[27]['SPAN ID'], out[30]['SPAN ID'])
        self.assertEqual(out[27]['PARENT SPAN'], out[30]['PARENT SPAN'])

    def test_restconf_nano_lsa55(self):
        self.assertEqual(*comp("restconf-nano-lsa55"))

    def test_operational_missing_commit_stop54(self):
        self.assertEqual(*comp("operational-missing-commit-stop54"))

    def test_operational_out_of_order_write_start_stop54(self):
        self.assertEqual(*comp("operational-out-of-order-write-start-stop54"))

    def test_create_nested54(self):
        """Ensure a nested span emitted from create callback is mapped to the
        parent transaction trace-id
        """
        out = readcsv("output/create-nested54.csv")
        self.assertEqual('sleep configured time', out[15]['MESSAGE'])
        self.assertEqual(out[0]['TRACE ID'], out[15]['TRACE ID'])

    def test_aborted54(self):
        """Test tlock heuristics for aborted transactions

        Ensure that the transaction lock tracking heuristics correctly deals
        with an aborted transaction. We can see in the output file how there is
        only a single 'holding transaction lock' span, as expected since the
        21693 transaction was aborted and thus never acquired the lock.

        A note on this test; aborted54 has been hand edited to avoid a NSO bug.
        In the 21693 transaction, NSO didn't emit a stop event for the validate
        span and as this breaks our parsing, this event was manually added as
        the point of this trace was to try and validate other functionality.
        Can't test / fix all at once....
        """
        self.assertEqual(*comp("aborted54"))


class TestOTelExport(unittest.TestCase):
    def setUp(self):
        self.maxDiff = None

    def test_simple(self):

        mem_exporter = InMemorySpanExporter()
        trace.set_tracer_provider(TracerProvider())
        trace.get_tracer_provider().add_span_processor(
            SimpleExportSpanProcessor(mem_exporter)
        )
        tracer = trace.get_tracer('opentelemetry-exporter')

        stream = ptrace.csv_ptrace_reader('input/simple54.csv')
        stream = ptrace.export_otel(tracer, stream)
        # getting this into a list makes the processing pipeline tick through
        # all events
        res = list(stream)
        spans_result = mem_exporter.get_finished_spans()
        # TODO: enable - this will fail unless you are in CEST timezone - it
        # needs to be made TZ independent
        #self.assertEqual(1602843574348000000, spans_result[0].start_time)

        # Ensure we see event on the 'write-start' span - it should have an
        # info event associated.
        self.assertEqual(1, len(spans_result[10].events))


def strip_metrics_host(data):
    """Strip away host from metrics Q data
    """
    for thing in data:
        if 'tags' in thing:
            thing['tags'].pop('host', None)

    return data


class TestInfluxExport(unittest.TestCase):
    def setUp(self):
        self.maxDiff = None

    def test_span_simple54(self):
        """Check that the span export starts up and gives data for the
        simple54.csv ptrace file
        """
        metrics_q = []
        stream = ptrace.csv_ptrace_reader('expected/simple54.csv')
        stream = ptrace.export_influx_span(metrics_q, stream)
        # getting this into a list makes the processing pipeline tick through
        # all events
        events = list(stream)
        res = strip_metrics_host(metrics_q)
        expected = strip_metrics_host([{'time': datetime.datetime(2020, 10, 16, 12, 19, 34, 352000), 'measurement': 'span', 'tags': {'host': 'nuc', 'name': 'grabbing transaction lock', 'service_type': ''}, 'fields': {'duration': 4.0, 'tid': '76', 'trace_id': 'bc89c32081d645aeac7df4ca6e42afc6', 'service': '', 'device': ''}}, {'time': datetime.datetime(2020, 10, 16, 12, 19, 34, 388000), 'measurement': 'span', 'tags': {'host': 'nuc', 'name': 'creating rollback file', 'service_type': ''}, 'fields': {'duration': 36.0, 'tid': '76', 'trace_id': 'bc89c32081d645aeac7df4ca6e42afc6', 'service': '', 'device': ''}}, {'time': datetime.datetime(2020, 10, 16, 12, 19, 34, 389000), 'measurement': 'span', 'tags': {'host': 'nuc', 'name': 'run transforms and transaction hooks', 'service_type': ''}, 'fields': {'duration': 0.0, 'tid': '76', 'trace_id': 'bc89c32081d645aeac7df4ca6e42afc6', 'service': '', 'device': ''}}, {'time': datetime.datetime(2020, 10, 16, 12, 19, 34, 389000), 'measurement': 'span', 'tags': {'host': 'nuc', 'name': 'mark inactive', 'service_type': ''}, 'fields': {'duration': 0.0, 'tid': '76', 'trace_id': 'bc89c32081d645aeac7df4ca6e42afc6', 'service': '', 'device': ''}}, {'time': datetime.datetime(2020, 10, 16, 12, 19, 34, 390000), 'measurement': 'span', 'tags': {'host': 'nuc', 'name': 'pre validate', 'service_type': ''}, 'fields': {'duration': 0.0, 'tid': '76', 'trace_id': 'bc89c32081d645aeac7df4ca6e42afc6', 'service': '', 'device': ''}}, {'time': datetime.datetime(2020, 10, 16, 12, 19, 34, 390000), 'measurement': 'span', 'tags': {'host': 'nuc', 'name': 'run validation over the changeset', 'service_type': ''}, 'fields': {'duration': 0.0, 'tid': '76', 'trace_id': 'bc89c32081d645aeac7df4ca6e42afc6', 'service': '', 'device': ''}}, {'time': datetime.datetime(2020, 10, 16, 12, 19, 34, 401000), 'measurement': 'span', 'tags': {'host': 'nuc', 'name': 'run dependency-triggered validation', 'service_type': ''}, 'fields': {'duration': 10.0, 'tid': '76', 'trace_id': 'bc89c32081d645aeac7df4ca6e42afc6', 'service': '', 'device': ''}}, {'time': datetime.datetime(2020, 10, 16, 12, 19, 34, 401000), 'measurement': 'span', 'tags': {'host': 'nuc', 'name': 'check configuration policies', 'service_type': ''}, 'fields': {'duration': 0.0, 'tid': '76', 'trace_id': 'bc89c32081d645aeac7df4ca6e42afc6', 'service': '', 'device': ''}}, {'time': datetime.datetime(2020, 10, 16, 12, 19, 34, 401000), 'measurement': 'span', 'tags': {'host': 'nuc', 'name': 'validate', 'service_type': ''}, 'fields': {'duration': 53.0, 'tid': '76', 'trace_id': 'bc89c32081d645aeac7df4ca6e42afc6', 'service': '', 'device': ''}}, {'time': datetime.datetime(2020, 10, 16, 12, 19, 34, 402000), 'measurement': 'span', 'tags': {'host': 'nuc', 'name': 'check data kickers', 'service_type': ''}, 'fields': {'duration': 0.0, 'tid': '76', 'trace_id': 'bc89c32081d645aeac7df4ca6e42afc6', 'service': '', 'device': ''}}, {'time': datetime.datetime(2020, 10, 16, 12, 19, 34, 402000), 'measurement': 'span', 'tags': {'host': 'nuc', 'name': 'write-start', 'service_type': ''}, 'fields': {'duration': 1.0, 'tid': '76', 'trace_id': 'bc89c32081d645aeac7df4ca6e42afc6', 'service': '', 'device': ''}}, {'time': datetime.datetime(2020, 10, 16, 12, 19, 34, 432000), 'measurement': 'span', 'tags': {'host': 'nuc', 'name': 'prepare', 'service_type': ''}, 'fields': {'duration': 29.0, 'tid': '76', 'trace_id': 'bc89c32081d645aeac7df4ca6e42afc6', 'service': '', 'device': ''}}, {'time': datetime.datetime(2020, 10, 16, 12, 19, 34, 469000), 'measurement': 'span', 'tags': {'host': 'nuc', 'name': 'holding transaction lock', 'service_type': ''}, 'fields': {'duration': 117.0, 'tid': '76', 'trace_id': 'bc89c32081d645aeac7df4ca6e42afc6', 'service': '', 'device': ''}}, {'time': datetime.datetime(2020, 10, 16, 12, 19, 34, 469000), 'measurement': 'span', 'tags': {'host': 'nuc', 'name': 'commit', 'service_type': ''}, 'fields': {'duration': 36.0, 'tid': '76', 'trace_id': 'bc89c32081d645aeac7df4ca6e42afc6', 'service': '', 'device': ''}}, {'time': datetime.datetime(2020, 10, 16, 12, 19, 34, 469000), 'measurement': 'span', 'tags': {'host': 'nuc', 'name': 'applying transaction', 'service_type': ''}, 'fields': {'duration': 121.0, 'tid': '76', 'trace_id': 'bc89c32081d645aeac7df4ca6e42afc6', 'service': '', 'device': ''}}, {'time': datetime.datetime(2020, 10, 16, 12, 19, 34, 469000), 'measurement': 'span', 'tags': {'host': 'nuc', 'name': 'transaction', 'service_type': ''}, 'fields': {'duration': 122.0, 'tid': '76', 'trace_id': 'bc89c32081d645aeac7df4ca6e42afc6', 'service': '', 'device': ''}}])
        self.assertEqual(expected, res)

    def test_transaction_simple54(self):
        """Check that the transaction export starts up and gives data for the
        simple54.csv ptrace file
        """
        metrics_q = []
        stream = ptrace.csv_ptrace_reader('input/simple54.csv')
        stream = ptrace.export_influx_transaction(metrics_q, stream)
        # getting this into a list makes the processing pipeline tick through
        # all events
        events = list(stream)
        res = strip_metrics_host(metrics_q)
        expected = [{
            'time': datetime.datetime(2020, 10, 16, 12, 19, 34, 469000),
            'measurement': 'transaction',
            'tags': {},
            'fields': {'grabbing transaction lock': 4000,
                       'creating rollback file': 36000,
                       'run transforms and transaction hooks': 0,
                       'mark inactive': 0,
                       'pre validate': 0,
                       'run validation over the changeset': 0,
                       'run dependency-triggered validation': 10000,
                       'check configuration policies': 0,
                       'validate': 53000,
                       'check data kickers': 0,
                       'write-start': 1000,
                       'prepare': 29000,
                       'holding transaction lock': 117000,
                       'commit': 36000,
                       'applying transaction': 121000,
                       'transaction': 122000,
                       'tid': '76'
            }
        }]
        self.assertEqual(expected, res)



def mow(delta=0):
    """Mock now - mow, pronounced Moooo, like a cow
    """
    dt_mow = datetime.datetime.now() + datetime.timedelta(milliseconds=delta)
    return int(dt_mow.strftime("%s%f"))

def mevent(ts, msg):
    """Create a mock event with given timestamp and message
    """
    event = {
        'timestamp': ts,
        'msg': msg
    }
    return event

def mock_notif_feeder():
    """Mock the notification API feed

    We deliberately introduce out-of-order message. d comes after e and should
    be rearranged by the notif_reader.
    """
    yield mevent(mow(), 'a')
    time.sleep(0.002)
    yield mevent(mow(), 'b')
    time.sleep(0.002)
    yield mevent(mow(), 'c')
    time.sleep(0.002)
    yield mevent(mow(), 'e')
    yield mevent(mow(-1), 'd')
    time.sleep(0.1)
    yield None


class TestNotifsReader(unittest.TestCase):
    def setUp(self):
        self.maxDiff = None

    def test_notifs_reader(self):
        """Test the reordering function of the notifs_reader

        We feed in the mock_notif_feeder which send an out-of-order event and
        we expect the notifs_reader to correct the order.
        """
        stream = ptrace.notifs_reader(mock_notif_feeder)
        res = list(stream)
        msgs = [e['msg'] for e in res]
        exp = ['a', 'b', 'c', 'd', 'e']
        self.assertEqual(exp, msgs)

if __name__ == "__main__":
    unittest.main()

# -*- mode: python; python-indent: 4 -*-
"""Main NSO package module

This is the entry point for the NSO package, it is invoked by NSO on startup
and sets up our ptrace processing pipeline based on configuration read from
NSO.
"""
import logging
import select
import socket

from influxdb import InfluxDBClient

from opentelemetry import metrics, trace
from opentelemetry.exporter import jaeger
from opentelemetry.exporter import prometheus
from opentelemetry.sdk.metrics import Counter, MeterProvider
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import SimpleExportSpanProcessor

from bgworker import background_process

import ncs
import _ncs

from . import dictsubscriber
from . import ptrace

# InfluxDBClient uses urllib3 which is quite chatty
logging.getLogger("urllib3").setLevel(logging.WARNING)



def decrypt_string(node, value):
    """Decrypts an encrypted tailf:aes-cfb-128-encrypted-string type leaf

    :param node: any maagic Node
    :param value: the encrypted leaf value
    """
    if value is None:
        return None

    if isinstance(node._backend, ncs.maagic._TransactionBackend):
        node._backend.maapi.install_crypto_keys()
    elif isinstance(node._backend, ncs.maagic._MaapiBackend):
        node._backend.install_crypto_keys()
    else:
        raise ValueError("Unknown MaagicBackend for leaf")

    return _ncs.decrypt(value) #pylint: disable=no-member


def OpenTelemetryExporter():
    """Function for running as NSO background worker to subscribe to NSO
    progress-trace notification and export to tracing system
    """
    log = logging.getLogger('opentelemetry-exporter')
    log.info('OpenTelemetry progress-trace exporter started')

    conf = dictsubscriber.DictSubscriber(
        app=None, log=log,
        subscriptions=[
            ("jaeger_base_url", "/progress:progress/opentelemetry-exporter:export/jaeger-base-url"),
            ("extra_tags", "/progress:progress/opentelemetry-exporter:export/extra-tags"),
        ]
    )

    metrics_q = []
    need_kicker_conf = False

    # Read in configuration from CDB
    with ncs.maapi.single_read_trans('opentelemetry-exporter', 'system') as t:
        root = ncs.maagic.get_root(t)
        export_config = root.progress.export

        cfg_influxdb = export_config.influxdb.exists()
        if cfg_influxdb:
            if ptrace.is_ipv6(export_config.influxdb.host):
                influx_host = f"[{export_config.influxdb.host}]"
            else:
                influx_host = export_config.influxdb.host

            influx_client = InfluxDBClient(
                influx_host,
                export_config.influxdb.port,
                export_config.influxdb.username,
                decrypt_string(root, export_config.influxdb.password),
                export_config.influxdb.database
            )
            influx_exporter = ptrace.InfluxdbExporter(influx_client, metrics_q)
            influx_exporter.start()

        cfg_jaeger = export_config.jaeger.exists()
        if cfg_jaeger:
            if ptrace.is_ipv6(export_config.jaeger.host):
                jaeger_host = f"[{export_config.jaeger.host}]"
            else:
                jaeger_host = export_config.jaeger.host

            jaeger_exporter = jaeger.JaegerSpanExporter(
                service_name="NSO",
                agent_host_name=jaeger_host,
                agent_port=export_config.jaeger.port,
            )

            trace.set_tracer_provider(TracerProvider())
            trace.get_tracer_provider().add_span_processor(
                SimpleExportSpanProcessor(jaeger_exporter)
            )

            tracer = trace.get_tracer('opentelemetry-exporter')

        cfg_logging = export_config.logging

        if ('ptrace-jaeger-influxdb' not in root.kickers.data_kicker
            or 'ptrace-jaeger-jaeger' not in root.kickers.data_kicker):
            need_kicker_conf = True

    # install kickers to restart ourselves on configuration changes
    if need_kicker_conf:
        with ncs.maapi.single_write_trans('opentelemetry-exporter-kicker', 'system') as t:
            root = ncs.maagic.get_root(t)
            kicker_influx = root.kickers.data_kicker.create('ptrace-export-influxdb')
            kicker_influx.monitor = '/progress:progress/opentelemetry-exporter:export/influxdb'
            kicker_influx.kick_node = './..'
            kicker_influx.action_name = 'restart'
            kicker_jaeger = root.kickers.data_kicker.create('ptrace-export-jaeger')
            kicker_jaeger.monitor = '/progress:progress/opentelemetry-exporter:export/jaeger'
            kicker_jaeger.kick_node = './..'
            kicker_jaeger.action_name = 'restart'
            t.apply()

    stream = ptrace.get_pipeline(ptrace.notifs_reader())

    # export to jaeger via opentelemetry library
    if cfg_jaeger:
        stream = ptrace.export_otel(tracer, stream)

    # export to influxdb
    if cfg_influxdb:
        stream = ptrace.export_influx_span(metrics_q, stream, conf=conf)
        stream = ptrace.export_influx_tlock(metrics_q, stream, conf=conf)
        stream = ptrace.export_influx_transaction(metrics_q, stream, conf=conf)

    # dummy consumer
    for event in stream:
        pass



class Main(ncs.application.Application):
    worker = None

    def setup(self):
        self.worker = background_process.Process(self, OpenTelemetryExporter, config_path='/progress:progress/opentelemetry-exporter:export/enabled')
        self.register_action('ptrace-export-restart', background_process.RestartWorker, init_args=self.worker)
        self.worker.start()

    def teardown(self):
        self.worker.stop()

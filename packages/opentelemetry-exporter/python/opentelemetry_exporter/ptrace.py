#!/usr/bin/env python3
"""NSO progress-trace processing functions

The overall design here is that of a data processing pipeline based on Python
generators. Multiple processing steps are connected together by passing in a
"stream" object, which is another generator.

A source or spout is used to ingest data into the pipeline. There are two types
of sources:
- csv_reader
  - reads progress-trace events from a CSV file
- get_ptrace_notifs
  - reads progress-trace events from the NSO notification API

Then one or more processing functions can be connected, for example, the
csv_writer consumes progress-trace events from the input stream and writes them
to an output CSV file. Conceptually, you could thus do:

csv_writer("output.csv", csv_reader("input.csv"))

In practice, that isn't possible because the csv_writer expects more columns
than provided by the csv_reader. That sounds weird, shouldn't input columns
always align with output columns? Well, over time there have been changes to
the NSO progress-trace CSV format. In NSO 5.4 there are 18 columns while in NSO
5.3 there were only 15. There is a processing function called uplift_53 which
will do its best to "uplift" events from the NSO 5.3 format to the NSO 5.4
format. However, that is still not enough, csv_writer expects 22 columns, of
which 4 are entirely new ones, namely:
- trace-id
- span-id
- parent
- structured

We think it would be a good idea for NSO to export at least the first three
extra columns but as it doesn't yet do that, we internally add them. This is
done by the function transmogrify(). It can be thought of as uplifting the
events to a next-generation version format. Having these extra columns makes it
trivial to implement an exporter. Look at how small and elegant the
export_otel() function is. This is thanks to the transmogrify() function doing
the heavy lifting of parsing the progress-trace events and presenting it in a
cleaner format. Similarly, the add_tlock_holder() function adds extra virtual
spans (well, emitting start & stop events) for when the transaction lock is
held. That should probably be done by NSO instead and perhaps it will in a
future release.

Note how due to Pythons syntax (which is really quite a common style of
programming language syntax) we build the pipeline "backwards", starting with
defining the output function and as we go deeper into the function arguments we
nest the calls to the earlier parts of the processing pipeline.

For example, this call in Python:

  csv_writer("output.csv", transmogrify(uplift_53(csv_reader("input.csv"))))

would typically be visualized like so:

    +------------+    +----------+    +--------------+    +------------+
    | csv_reader | -> | uplift53 | -> | transmogrify | -> | csv_writer |
    +------------+    +----------+    +--------------+    +------------+

Also note that the pipeline can never fork out. It would be natural to build a
pipeline with two exporters (to Jaeger and InfluxDB) connected after the last
non-exporter processing step, like so:

                                            +--------+
                                        +-> | jaeger |
    +------------+    +--------------+ /    +--------+
    | csv_reader | -> | transmogrify |-
    +------------+    +--------------+ \    +--------+
                                        +-> | influx |
                                            +--------+

However, this is simply not possible while using Python generators the way we
are. Instead, the exporters are chained after each other.

    +------------+    +--------------+    +--------+    +--------+
    | csv_reader | -> | transmogrify | -> | jaeger | -> | influx |
    +------------+    +--------------+    +--------+    +--------+
                                               |             |
                                               v             v
                                        exported data   exported data

This means each exporter must also still act as a generator and yield events so
that we can in fact hook up more things after it.
"""
import csv
import datetime
import logging
import random
import re
import secrets
import socket
import sys
import threading
import time
import uuid

from collections import OrderedDict

import parsedatetime

from influxdb import InfluxDBClient

from opentelemetry import metrics, trace as trace_api
from opentelemetry.exporter import jaeger
from opentelemetry.sdk.metrics import Counter, MeterProvider, UpDownCounter, ValueRecorder
from opentelemetry.sdk.metrics.export.aggregate import HistogramAggregator, MinMaxSumCountAggregator
from opentelemetry.sdk.metrics.view import View, ViewConfig
from opentelemetry.sdk import trace as trace_sdk
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import SimpleExportSpanProcessor

# InfluxDBClient uses urllib3 which is quite chatty
logging.getLogger("urllib3").setLevel(logging.WARNING)


def is_ipv6(address):
    """Check if an address is an IPv6 address
    """
    try:
        socket.inet_pton(socket.AF_INET6, address)
    except (socket.error, UnicodeEncodeError):
        return False
    return True


# This is a mapping of notif API header fields to the 5.4 CSV format fields
header_notif2csv = {
    'type': 'EVENT TYPE',
    'trace_id': 'TRACE ID',
    'span_id': 'SPAN ID',
    'parent': 'PARENT SPAN',
    'timestamp': 'TIMESTAMP',
    'duration': 'DURATION',
    'usid': 'SESSION ID',
    'tid': 'TRANSACTION ID',
    'datastore_name': 'DATASTORE',
    'context': 'CONTEXT',
    'subsystem': 'SUBSYSTEM',
    'phase': 'PHASE',
    'service': 'SERVICE',
    'service_phase': 'SERVICE PHASE',
    'cqid': 'COMMIT QUEUE ID',
    'node': 'NODE',
    'device': 'DEVICE',
    'device_phase': 'DEVICE PHASE',
    'package': 'PACKAGE',
    'msg': 'MESSAGE',
    'annotation': 'ANNOTATION',
    'component': 'COMPONENT',
    'state': 'STATE',
    'struct': 'STRUCTURED'
}
# This maps from the 5.4 CSV format to the notif fields
header_csv2notif = {val: key for key, val in header_notif2csv.items()}
# NSO 5.3 calls it "TID" instead of "TRANSACTION ID", so we simply add another
# mapping.
# NOTE: if there are more differences in future, consider separate dict
header_csv2notif["TID"] = "tid"

def prettify(event):
    """Prettify event for printing by converting timestamp to human readable time

    This makes it considerable easier to debug things.
    """
    res = event.copy()
    ts = datetime.datetime.fromtimestamp(event['timestamp']/1000000)
    res['htimestamp'] = ts.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3]
    return res


def raw_csv_reader(filename):
    """Read ptrace events from CSV file

    Internally we deal with things in the notification API format and so we
    need to convert keys and values to the notif API format before outputting.
    """
    log = logging.getLogger('raw_csv_reader')
    with open(filename) as csvfile:
        csvreader = csv.DictReader(csvfile)
        for row in csvreader:
            event = {}
            for key, val in row.items():
                if key == 'TIMESTAMP':
                    # convert timestamp
                    ts = datetime.datetime.strptime(val, '%Y-%m-%dT%H:%M:%S.%f')
                    val = int(ts.strftime("%s%f"))
                if key == 'DURATION':
                    # Internal duration is in microseconds, convert from
                    # seconds. Like we have a transaction taking 122
                    # milliseconds, which is written as 0.122 in the test CSV
                    # file and internally it is 122000 (i.e. µs - microseconds)
                    try:
                        val = float(val)*1000000.0
                    except:
                        pass
                event[header_csv2notif[key]] = val

            yield event


def csv_reader(filename):
    """A buffering CSV reader to reorder events for monotonic time

    The raw CSV file might not have events ordered by time. We fix that by
    introducing a buffer and comparing the timestamps of events to the previous
    events timestamp, possibly reordering events in the buffer before yielding.

    We buffer 100 messages, which ought to be enough for correctly ordering
    things since we don't expect the out-of-ordering to be that large.

    We can't guarantee perfectly correct ordering since sometimes multiple
    events have the same timestamp and then ordering between them is not
    possible to determine by the timestamp alone. Understanding the events can
    act as a hint in which order they ought to happen, but we don't attempt to
    implement that here.
    """
    buf = []
    csv_stream = raw_csv_reader(filename)
    last_ts = None
    for event in csv_stream:
        if last_ts is None:
            buf.append(event)
        elif event['timestamp'] >= last_ts:
            buf.append(event)
        else:
            # need to place this earlier, go backwards in buffer until we
            # find an event that is older than ours
            for pos in range(1, len(buf)+1):
                if event['timestamp'] >= buf[-pos]['timestamp']:
                    buf.insert(-(pos-1), event)
                    break
        last_ts = event['timestamp']

        if len(buf) > 100:
            yield buf.pop(0)

    # on end of input stream we export the rest in buf
    for e in buf:
        yield e


def csv_writer(filename, stream):
    """Write ptrace events to CSV file

    Internally we deal with things in the notification API format and so we
    need to convert certain keys and values to the CSV format before writing.
    """
    csv_fields = ['EVENT TYPE', 'TRACE ID', 'SPAN ID', 'PARENT SPAN', 'TIMESTAMP', 'DURATION', 'SESSION ID', 'TRANSACTION ID', 'DATASTORE', 'CONTEXT', 'SUBSYSTEM', 'PHASE', 'SERVICE', 'SERVICE PHASE', 'COMMIT QUEUE ID', 'NODE', 'DEVICE', 'DEVICE PHASE', 'PACKAGE', 'MESSAGE', 'ANNOTATION', 'COMPONENT', 'STATE', 'STRUCTURED']
    with open(filename, "w", newline="") as csvfile:
        csvwriter = csv.DictWriter(csvfile, csv_fields)
        csvwriter.writeheader()
        for event in stream:
            row = {}
            for key, val in event.items():
                if key == 'timestamp':
                    # convert timestamp from internal microsecond timestamp to
                    # "human readable", notably with 3 decimal millisecond
                    # instead of 6 digit 0 padded micro second
                    ts = datetime.datetime.fromtimestamp(val/1000000)
                    val = ts.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3]
                if key == 'duration':
                    # Internal duration is in microseconds, convert to seconds.
                    # Like we have a transaction taking 122 milliseconds, which
                    # is written as 0.122 in the test CSV file and internally
                    # it is 122000 (i.e. µs - microseconds)
                    try:
                        numval = float(val)/1000000.0
                        # NSO always writes with 3 decimal precision, so let's
                        # stick to the same style
                        val = f"{numval:.3f}"
                    except:
                        pass
                row[header_notif2csv[key]] = val

            csvwriter.writerow(row)



def get_ptrace_notifs():
    """Generator that yields NSO progress-trace events, coming from the NSO
    notification API that we subscribed to.

    This is a raw interface not intended for direct use. You probably want the
    notifs_reader instead!
    """
    import select
    import ncs
    from _ncs import events
    import _ncs

    # set up listener for NSO notifications
    event_sock = socket.socket()
    mask = events.NOTIF_PROGRESS
    noexists = _ncs.Value(init=1, type=_ncs.C_NOEXISTS)
    notif_data = _ncs.events.NotificationsData(heartbeat_interval=1000, health_check_interval=1000, stream_name='whatever', start_time=noexists, stop_time=noexists, verbosity=ncs.VERBOSITY_VERY_VERBOSE)
    events.notifications_connect2(event_sock, mask, ip='127.0.0.1', port=ncs.NCS_PORT, data=notif_data)

    while True:
        (readables, _, _) = select.select([event_sock], [], [], 0.1)
        for readable in readables:
            if readable == event_sock:
                event_dict = events.read_notification(event_sock)
                pev = event_dict['progress']

                type_map = { 1: 'start', 2: 'stop', 3: 'info' }
                pev['type'] = type_map[pev['type']]

                yield pev
        if readables == []:
            # we feed dummy events if there are no real events, this is so
            # that the downstream consumer of events is always "woken up"
            # with the arrival of a new event, at least every 0.1 seconds
            yield None


def notifs_reader(notif_feeder=get_ptrace_notifs):
    """NSO progress-trace reader for the internal notification API

    Use this to get progress trace notifications!

    It introduces a buffer in order to reorder events according to their
    timestamp. This makes parsing considerably easier. It is built to work in
    tandem with the get_ptrace_notifs() function, as it guarantees to yield an
    event every 100ms at a minimum and thus we can keep our guarantee.

    Somewhat surprisingly, the NSO notification API might not yield
    notifications in the order reflected by their timestamps. For example, a
    newly arrived message could have a timestamp that is older than the
    previous message. At least, this is what can be observed in the
    progress-trace CSV files and I believe the same thing happens over the
    notifications API.

    It is considerably easier to parse messages that follow monotonic time, so
    we aim to achieve monotonic time by reordering messages. For this we keep a
    buffer of messages for 100 ms, which gives us a chance to reorder messages.
    It is highly unlikely messages would be reordered across more than 100ms.
    """
    log = logging.getLogger('notifs_reader')

    buf = []
    last_ts = None
    for event in notif_feeder():
        # yield events older than 100ms
        dt_cut = datetime.datetime.now() - datetime.timedelta(milliseconds=100)
        cut = int(dt_cut.strftime("%s%f"))
        while True and len(buf) > 0:
            e = buf[0]
            if e['timestamp'] < cut:
                yield e
            else:
                break
            buf.pop(0)

        # skip the dummy events, they just serve to wake us up so we can yield
        # old events
        if event is None:
            continue

        if last_ts is None:
            buf.append(event)
        elif event['timestamp'] >= last_ts:
            buf.append(event)
        else:
            # need to place this earlier, go backwards in buffer until we
            # find an event that is older than ours
            for pos in range(1, len(buf)+1):
                if event['timestamp'] >= buf[-pos]['timestamp']:
                    buf.insert(-(pos-1), event)
                    break

        last_ts = event['timestamp']

    # when end of feed is reached, we come here, and yield the remaining buffer
    for e in buf:
        yield e


def uplift_53(stream):
    """Uplift progress-trace format from < 5.4 format to 5.4 format
    """
    log = logging.getLogger('uplift_53')
    # Names of spans that have balanced start and stop messages
    span_names = {
        'applying FASTMAP reverse diff-set',
        'applying transaction',
        'check configuration policies',
        'check data kickers',
        'create',
        'creating rollback file',
        'grabbing transaction lock',
        'mark inactive',
        'post-modification',
        'pre validate',
        'pre-modification',
        'run dependency-triggered validation',
        'run pre-transform validation',
        'run service',
        'run transforms and transaction hooks',
        'run validation over the changeset',
        'saving FASTMAP reverse diff-set and applying changes',
    }

    # Known info messages. Anything not in here and not matching a list of
    # regexps will result in a warning - possibly something we want to look
    # into.
    known_info = {
        'all commit subscription notifications acknowledged',
        'commit',
        'conflict deleting zombie, adding re-deploy to sequential side effect queue',
        'nano service deleted',
        'prepare',
        're-deploy merged in queue',
        're-deploy queued',
        'received commit from all (available) slaves',
        'received prepare from all (available) slaves',
        'releasing device lock',
        'releasing transaction lock',
        'send NED close',
        'send NED commit',
        'send NED connect',
        'send NED get-trans-id',
        'send NED initialize',
        'send NED is-alive',
        'send NED noconnect',
        'send NED persist',
        'send NED prepare',
        'send NED prepare-dry',
        'send NED reconnect',
        'send NED revert',
        'send NED show',
        'send NED show-partial',
        'send NED uninitialize',
        'sending confirmed commit',
        'sending confirming commit',
        'sending edit-config',
        'transaction empty',
        'write-start',
        'zombie deleted',
    }

    known_info_re = {
        'transaction lock queue length:',
        'SNMP connect to',
        '(SNMP|SSH) connecting to',
        'reuse SSH connection',
        'SNMP USM engine id',
        'evaluated behaviour tree pre-condition',
        'component {',
        'delivering commit subscription notifications',
    }

    start_seen = {}

    for event in stream:
        durm = re.search(r'(.*) \[([0-9]+) ms\]$', event['msg'])
        if durm:
            if event['duration'] != '':
                log.warning(f"got duration in msg but duration field set: {event}")
            event['msg'] = durm.group(1)
            event['duration'] = durm.group(2)

        if 'type' not in event:
            if re.match('(entering|leaving) (.+) phase', event['msg']):
                m = re.match('(entering|leaving) (.+) phase', event['msg'])
                msg_map = {'entering': 'start', 'leaving': 'stop'}
                event['type'] = msg_map[m.group(1)]
                event['msg'] = m.group(2)

            elif event['msg'].endswith('...'):
                event['type'] = 'start'
                event['msg'] = event['msg'].replace('...', '')
                start_seen[event['msg']] = True

            elif re.search(' (done|error|ok)$', event['msg']):
                event['type'] = 'stop'
                event['msg'] = re.sub(' (done|error|ok)$', '', event['msg'])
                if event['msg'] in start_seen:
                    del start_seen[event['msg']]

            elif event['msg'] in span_names:
                if event['msg'] in start_seen:
                    event['type'] = 'stop'
                    del start_seen[event['msg']]
                else:
                    event['type'] = 'start'
                    start_seen[event['msg']] = True
            else:
                event['type'] = 'info'
                if [ i for i in known_info_re if re.match(i, event['msg'])]:
                    pass
                elif (re.match('evaluated behaviour tree', event['msg'])
                        or re.match('component.*state', event['msg'])
                        ):
                    pass
                elif re.search('NED error reply', event['msg']):
                    # TODO: consider info message for now but this should
                    # probably be considered a traceback? I think opentelemetry
                    # has special kind of handling for that kind of thing.
                    pass
                elif event['msg'] not in known_info:
                    log.warning(f"WARNING: unknown message, assuming info: {event['msg']}")

        if event['msg'] == 'service create':
            event['msg'] = 'create'

        yield event



def transmogrify(stream):
    """Transmogrify the current (5.4) NSO progress-trace format into its ideal
    shape We can think of this as uplifting the format to NSO v5.future, it's
    like the next-generation version of the format.

    Three new columns are added to the output:
    - trace-id
    - span-id
    - parent
    - struct

    The first three of which are common concepts in the tracing world and which
    make the correlation in later processing stages trivial. The struct field
    is for keeping structured data, like in a key value format.

    Without a span-id we have to match up the start and stop events. We have
    classically done this by a depth based matching but are here testing a
    match on message value per transaction and dimension. The dimension is like
    a sub-key under a transaction id, in which events appears to be happening
    sequentially.

    There is generally a 1:1 mapping between NSO transactions and traces. This
    is however not true for trans-in-trans, i.e. sub-transactions, most
    commonly used for service create where we map the child transaction to the
    same trace-id as the parent transaction. This means progress-trace spans
    emitted inside of create will belong to the same trace as for the whole
    transaction that created a service, which looks much better!
    """
    log = logging.getLogger('transmogrify')
    # this contains depth and spans keyed per tid
    traces = {}

    # keep track of currently running service create spans
    srv_create = {}

    def inner(stream):
        for event in stream:
            tkey = (event['usid'], event['tid'])
            # If this is the first time we see this tid, we do a quick check to try
            # to determine if this is a trans-in-trans used for service create, in
            # which case we attempt to map it to its parent transaction trace id
            # instead! We do this by checking the service attribute and if it
            # exists in the srv_create dict, it means we are currently in the
            # create span of that service, in which case this event must be part of
            # a service create trans-in-trans!
            if (tkey not in traces
                and event.get('service', None) in srv_create):
                tkey = srv_create[event['service']]

            # Check if it's an existing transaction we know about
            if tkey not in traces:
                # create struct for our transaction, keyed by tid
                if event['msg'] in {'restconf edit', 'sync-from'}:
                    # no need to wrap these in fake root transactions
                    pass
                elif event['tid'] == '-1':
                    pass
                else:
                    # Create virtual root span
                    root_span = event.copy()
                    root_span['type'] = 'start'
                    root_span['trace_id'] = uuid.uuid4()
                    root_span['span_id'] = secrets.token_hex(8)
                    root_span['parent'] = ''
                    root_span['msg'] = 'transaction'
                    root_span['struct'] = {}
                    yield root_span
                    traces[tkey]['fake_root'] = root_span

            yield event

            trace = traces[tkey]
            # We determine the end of the transaction when there are no locks held
            # and there are no open spans. Lacking an explicit root span (what we
            # call our 'transaction' span), this is so far the best heuristics we
            # could come up with.
            open_spans = 0
            for dim in trace['dimensions'].values():
                if len(dim['open_spans']) == 1 and 'transaction' in dim['span_by_msg']:
                    pass
                else:
                    open_spans += len(dim['open_spans'])

            stop = False
            if open_spans == 0 and len(locks) == 0:
                stop = True
            elif event['type'] == 'stop' and event['msg'] == trace['start_msg']:
                log.error(f"End of transaction detected by start_msg ({event['msg']}) but not at depth 0 / locks kept for {tkey}\n{our_dim['span_by_msg']}")
                stop = True

            if stop:
                if trace['fake_root'] is not None:
                    root_span = trace['fake_root']
                    # Create event for the root span stop event
                    root_stop = root_span.copy()
                    root_stop['type'] = 'stop'
                    root_stop['timestamp'] = event['timestamp']
                    root_stop['duration'] = (event['timestamp']-root_span['timestamp'])
                    yield root_stop
                del traces[tkey]


    for event in inner(stream):
        tkey = (event['usid'], event['tid'])
        # If this is the first time we see this tid, we do a quick check to try
        # to determine if this is a trans-in-trans used for service create, in
        # which case we attempt to map it to its parent transaction trace id
        # instead! We do this by checking the service attribute and if it
        # exists in the srv_create dict, it means we are currently in the
        # create span of that service, in which case this event must be part of
        # a service create trans-in-trans!
        if (tkey not in traces
            and event.get('service', None) in srv_create):
            tkey = srv_create[event['service']]

        # Check if it's an existing transaction we know about
        if tkey not in traces:
            if event.get('trace_id', '') != '':
                trace_id = event['trace_id']
            else:
                trace_id = uuid.uuid4()

            traces[tkey] = {
                'trace_id': trace_id,
                'dimensions': {},
                'locks': {},
                'fake_root': None,
                'start_msg': None,
                'first_event': event.copy(),
                'top_dim': event.get('device', '')
            }
        trace = traces[tkey]
        locks = trace['locks']
        event['trace_id'] = trace['trace_id']
        event['struct'] = {}

        # we use the concept of a 'dimension' to separate spans
        # NSO will essentially do sequential processing of most things except
        # for device interaction. Most device interaction is done concurrently
        # between devices, though sequentially for each device. Thus, when
        # tracking events and their structure, we use the device name as a
        # "dimension" to keep concurrent operations apart. For non-device
        # spans, we use an empty string '' as our dimension.
        dim_key = event.get('device', '')

        if dim_key not in trace['dimensions']:
            trace['dimensions'][dim_key] = {
                'open_spans': {},
                'span_by_msg': {}
            }
        our_dim = trace['dimensions'][dim_key]

        if event.get('parent', '') == '':
            # special handling of device spans that we want to attach to a
            # non-device span as parent
            if (dim_key != ''
                    and '' in trace['dimensions']
                    and trace['dimensions']['']['open_spans'] != {}
                    and list(trace['dimensions']['']['open_spans'].values())[-1]['msg'] in ['commit', 'prepare']):
                event['parent'] = list(trace['dimensions']['']['open_spans'].keys())[-1]
                parent_spans = {}
            elif len(trace['dimensions'][dim_key]['open_spans']) == 0:
                # no open spans in dimension? means we are at depth 0 in our dim...

                # are we the top dimension?
                if dim_key == trace['top_dim']:
                    event['parent'] = None
                    parent_spans = {}
                else:
                    # otherwise, attach to the top dimension
                    parent_spans = trace['dimensions'][trace['top_dim']]['open_spans']
            else:
                # there are open spans in our dimension
                parent_spans = trace['dimensions'][dim_key]['open_spans']

            for span_id, span in reversed(list(parent_spans.items())):
                if span['msg'] in {'holding transaction lock'}:
                    # we don't think the holding transaction lock span is eligible
                    # to be a parent of other spans, thus ignoring it here
                    continue
                event['parent'] = span_id
                break

            if event.get('parent', '') == '':
                log.error(f"Unable to identify parent for {event}")

        # keep track of transaction lock
        if event['msg'] == 'holding transaction lock':
            if event['type'] == 'start':
                if '' in locks:
                    log.error("start/holding transaction lock: tlock already held")
                locks[''] = True
            elif event['type'] == 'stop':
                if '' not in locks:
                    log.error("stop/holding transaction lock: tlock not held")
                locks.pop('', None)

        # keep track of create per service
        if event['msg'] == 'create' and event.get('service', '') != '':
            if event['type'] == 'start':
                srv_create[event['service']] = tkey

            if event['type'] == 'stop':
                srv_create.pop(event['service'], None)

        if event['type'] == 'start': # start of a span
            # sanity
            if event['msg'] in our_dim['span_by_msg']:
                log.error(f"span dim collision on {dim_key}/{event['msg']}\n{prettify(event)}\nother event: {prettify(our_dim['span_by_msg'][event['msg']])}")
            # keep a list of open spans in this transaction
            our_dim['open_spans'][event['span_id']] = event
            our_dim['span_by_msg'][event['msg']] = event
        elif event['type'] == 'stop': # end of a span
            if (event['msg'] == 'taking device lock'
                and 'annotation' in event
                and event['annotation'] == ''):
                locks[event['device']] = True

            # attempt matching up stop event to start event through span_id, if
            # set, otherwise we do it by message
            # TODO: guess the span_id match should not be by dimension but per
            # trace?
            if event.get('span_id', '') != '':
                if event['span_id'] in our_dim['open_spans']:
                    match = our_dim['open_spans'][event['span_id']]
                else:
                    log.error(f"Unable to match up stop event based on span_id {event}")
                    log.error(trace)
            else:
                try:
                    match = our_dim['span_by_msg'][event['msg']]
                except KeyError:
                    log.error(f"Unable to match up stop event - discarding: {event}")
                    continue
                event['span_id'] = match['span_id']

            event['parent'] = match['parent']

            our_dim['open_spans'].pop(event['span_id'], None)
            our_dim['span_by_msg'].pop(event['msg'], None)

        elif event['type'] == 'info':
            if event['msg'] == 'releasing device lock':
                locks.pop(event['device'], None)

            match = None
            # match up with last open span for our dimension
            if len(our_dim['open_spans']) > 0:
                match_key = list(our_dim['open_spans'].keys())[-1]
                match = our_dim['open_spans'][match_key]

            if match is None:
                log.error(f"Unable to associate info event with last open span. Event: {prettify(event)}")
            else:
                # inherit span_id & parent span from matched up span
                event['span_id'] = match['span_id']
                event['parent'] = match['parent']
        else:
            raise ValueError(f"Unhandled progress-trace event type {event['type']}")

        yield event

        # something is wrong if this ballons in size, 1000 is a very high limit
        # given how NSO normally operates, so this *must* be a bug somewhere.
        # Most likely unbalanced start / stop messages.
        if len(traces) > 1000:
            log.error(f"Excessive transaction state({len(traces)}). Pruning oldest.")
            traces = prune_oldest(traces, 500)

    # When we exit the main iteration of the stream, it means we've processed
    # all events. We shouldn't have any state left!
    # If we are started in the middle of a trace stream so that we've missed
    # some start messages or are cut off before we see some stop messages, we
    # might have leftover state. When run as a NSO package we might want to be
    # more tolerant to such scenarios and simply ignore these failures.
    if len(traces) > 0:
        log.error("Unclean transaction state at end. Trailing transactions:")
        log.error(traces)


def prune_oldest(traces, prune_number):
    """Prune the oldest traces

    Prunes away prune_number number of the oldest traces from traces. Useful to
    clean up the state store in transmogrify. Note how we rely on the trace
    storage format used in the transmogrify function.
    """
    log = logging.getLogger('prune_oldest')
    sorted_traces = sorted(traces.items(), key=lambda item: item[1]['first_event']['timestamp'])
    for i, item in enumerate(sorted_traces):
        key, trace = item
        log.warning(f"Pruning: {traces[key]}")
        traces.pop(key, None)
        if i >= prune_number:
            break

    return traces


def fix_dev_snapshot(stream):
    """Correct device snapshot messages

    This is fixed in NSO 5.5 and later through ENG-24451
    """
    for event in stream:
        m = re.match("create device (.+) snapshot", event['msg'])
        if m:
            event['msg'] = 'create snapshot'
            event['device'] = m.group(1)
        yield event


def fix_oper_commit_stop(stream):
    """Correct transactions on operational datastore that are missing the
    commit stop event

    We track transactions on the operational datastore and if we haven't seen a
    commit message before the 'applying transaction' stop message, we emit a
    commit stop event. The generated fake event gets the same timestamp as the
    'applying transaction' stop message. We only track transactions starting
    with an 'applying transaction' message. There are also other transactions
    on the operational datastore, like doing connect on a device but as such
    transactions don't have a commit step we don't track them.

    See ENG-25236
    """
    log = logging.getLogger('fix_oper_commit_stop')
    transactions = {}
    for event in stream:
        tkey = (event['usid'], event['tid'])
        if ('datastore_name' in event
            and event['datastore_name'] == 'operational'
            and event['type'] == 'start'
            and event['msg'] == 'applying transaction'):
            if tkey in transactions:
                log.error(f"Seeing start of transaction [start/applying transaction] but transaction already in state for {event['tid']}")
            # track operational transactions starting with an 'applying
            # transaction' event
            transactions[tkey] = {
                'commit_start_event': None,
                'commit_stop_seen': False
            }

        if tkey in transactions:
            if event['type'] == 'start' and event['msg'] == 'commit':
                transactions[tkey]['commit_start_event'] = event
            elif event['type'] == 'stop' and event['msg'] == 'commit':
                transactions[tkey]['commit_stop_seen'] = True
            elif event['type'] == 'stop' and event['msg'] == 'applying transaction':
                # Not all transactions have a commit event. The typical example
                # being if they are empty. At the end of the transaction, i.e.
                # at stop 'applying transaction' event, if we have seen the
                # commit start event but have NOT seen a commit stop event, we
                # emit a fake one.
                if (transactions[tkey]['commit_start_event'] is not None
                    and transactions[tkey]['commit_stop_seen'] is False):
                    # we've reached the end of the transaction but haven't seen the
                    # stop commit message, so we insert a fake one
                    commit_stop = event.copy()
                    commit_stop['msg'] = 'commit'
                    start_event = transactions[tkey]['commit_start_event']
                    commit_stop['duration'] = (commit_stop['timestamp'] - start_event['timestamp'])
                    yield commit_stop
                del(transactions[tkey])
        yield event

        # if this balloons in size, something is wrong, likely a bug
        if len(transactions) > 1000:
            log.error("Excessive transaction state:")
            log.error(transactions)


def fix_ooo_write_start_stop(stream):
    """Correct event order for write-start stop event on operational datastore

    Some transactions on the operational datastore appear to get the stop event
    for 'applying transaction' and 'write-start' mixed up, e.g.:

        EVENT TYPE,TIMESTAMP,DURATION,SESSION ID,TRANSACTION ID,DATASTORE,CONTEXT,SUBSYSTEM,PHASE,SERVICE,SERVICE PHASE,COMMIT QUEUE ID,NODE,DEVICE,DEVICE PHASE,PACKAGE,MESSAGE,ANNOTATION
        start,2020-10-16T10:30:58.474,,720571,176697257,operational,system,,,,,,,,,,"applying transaction",
        start,2020-10-16T10:30:58.474,,720571,176697257,operational,system,,write-start,,,,,,,,"write-start",
        info,2020-10-16T10:30:58.475,,720571,176697257,operational,system,cdb,write-start,,,,,,,,"write-start",
        start,2020-10-16T10:30:58.475,,720571,176697257,operational,system,,,,,,,,,,"check data kickers",
        stop,2020-10-16T10:30:58.475,0.000,720571,176697257,operational,system,,,,,,,,,,"check data kickers",
        info,2020-10-16T10:30:58.480,,720571,176697257,operational,system,,,,,,,,,,"transaction empty",
        stop,2020-10-16T10:30:58.480,0.006,720571,176697257,operational,system,,,,,,,,,,"applying transaction","empty"
        stop,2020-10-16T10:30:58.480,0.006,720571,176697257,operational,system,,write-start,,,,,,,,"write-start",

    We fix this by reordering the last two events.

    We track transactions on the operational datastore that starts with an
    'applying transaction' event. If we have seen the 'write-start' start
    message we should also expect to see the corresponding stop event for
    'write-start' before the end of the transaction ('applying transaction'
    stop event). If we haven't seen the 'write-start' stop event by the time we
    see the stop event for 'applying transaction' we will hold the message and
    emit them in the revers order, i.e. get the correct order.

    See ENG-25241
    """
    log = logging.getLogger('fix_ooo_write_start_stop')
    transactions = {}
    for event in stream:
        tkey = (event['usid'], event['tid'])
        if ('datastore_name' in event
            and event['datastore_name'] == 'operational'
            and event['type'] == 'start'
            and event['msg'] == 'applying transaction'):
            if tkey in transactions:
                log.error(f"Seeing start of transaction [start/applying transaction] but transaction already in state for {event['tid']}")
            # track operational transactions starting with an 'applying
            # transaction' event
            transactions[tkey] = {
                'applying_transaction_stop_event': None,
                'write_start_stop_seen': False,
                'write_start_start_seen': False
            }

        if tkey in transactions:
            if event['type'] == 'start' and event['msg'] == 'write-start':
                transactions[tkey]['write_start_start_seen'] = True
            elif event['type'] == 'stop' and event['msg'] == 'write-start':
                transactions[tkey]['write_start_stop_seen'] = True
                # if we haven't yet seen the 'applying transaction' stop event,
                # it means messages are in the right order
                if transactions[tkey]['applying_transaction_stop_event'] is None:
                    pass
                else:
                    # we are on the write-start message, which is out-of-order.
                    # Emit the write-start first followed by the stop 'applying
                    # transaction' event. This means the end of the transaction
                    # so remove our state and explicitly skip the normal yield,
                    # which would otherwise result in duplicate event.
                    yield event
                    yield transactions[tkey]['applying_transaction_stop_event']
                    del(transactions[tkey])
                    continue

            elif event['type'] == 'stop' and event['msg'] == 'applying transaction':
                # Seeing the stop event for 'applying transaction' means we are
                # now at the end of the transaction. If we have seen the
                # write-start start message but not yet the write-start stop
                # message, it likely means out-of-order.
                if (transactions[tkey]['write_start_start_seen'] is True
                    and transactions[tkey]['write_start_stop_seen'] is False):
                    transactions[tkey]['applying_transaction_stop_event'] = event
                    # explicitly do not yield the event
                    continue

                del(transactions[tkey])

        yield event

        # if this balloons in size, something is wrong, likely a bug
        if len(transactions) > 1000:
            log.error("Excessive transaction state:")
            log.error(transactions)


def fix_restconf_type(stream):
    """Various restconf events should use start/stop type

    NSO emits some RESTCONF related events, like 'restconf edit' around a
    RESTCONF transaction and while they represent a span, they have the type
    'info'. We change to 'start'/'stop' since that is what it should be!
    ENG-25328 for changing the event type.
    """
    log = logging.getLogger('fix_restconf_type')
    for event in stream:
        if (event['type'] == 'info'
            and event['msg'] in (
                'restconf edit',
                'restconf get'
            )):
            if event['duration'] == '':
                event['type'] = 'start'
            else:
                event['type'] = 'stop'
        yield event


def fix_nano_events(stream):
    """Separate instance data from nano-service progress-trace events

    Nano-service related progress-trace events contain variable instance data
    like the component name and name of the state.
    """
    log = logging.getLogger('fix-nano-events')
    for event in stream:
        # saving FASTMAP nano reverse diff-set and applying changes component {ncs:self self}: state ncs:init
        # component {ncs:self self}: state ncs:init status change not-reached -> reached
        # component {ncs:self self}: state ncs:ready precondition evaluated to false
        # component {ncs:self self}: state abc:dhcp
        m = re.match("(?P<msg1>.*?) {(?P<component>[^}]+)}: state (?P<state>[^ ]+)(?P<msg2>.*?)$", event['msg'])
        if m:
            event['msg'] = re.sub(" component state$", "", f"{m.group('msg1')} state{m.group('msg2')}")
            event['component'] = m.group('component')
            event['state'] = m.group('state')

        m = re.match("(?P<msg>executing side effect) for (?P<kp>.+) op (?P<op>[^ ]+)$", event['msg'])
        if m:
            event['msg'] = f"{m.group('msg')} on {m.group('op')}"
            # break down keypath to service, component & state
            n = re.match("(?P<service>.*)/plan/component{(?P<component>[^}]+)}/state{(?P<state>[^}]+)}", m.group("kp"))
            if n:
                event['component'] = n.group('component')
                event['state'] = n.group('state')
            else:
                log.error(f"Unable to parse keypath {m.group('kp')}")

        yield event


def fix_check_conflicts(stream):
    """Separate variable instance data in 'check conflicts' events
    """
    for event in stream:
        # check conflicts (with transaction 1234)
        m = re.match("check conflicts \(with transaction ([0-9]+)\)", event['msg'])
        if m:
            event['msg'] = 'check conflicts'
            event['other_tid'] = m.group(1)
        yield event


def add_span_id(stream):
    """Add a span-id field to events lacking one
    """
    for event in stream:
        if event['type'] == 'start':
            if ('span_id' not in event or event['span_id'] == ''):
                event['span_id'] = secrets.token_hex(8)

        yield event



def add_tlock_holder(stream):
    """Adds virtual span for when transaction-lock is held
    """
    log = logging.getLogger('add-tlock-holder')
    grabbing_lock = {}
    # the span that is holding the transaction-lock or None, when it is not
    # held / we don't know (we might have started up in the middle of an
    # ongoing transaction)
    hold_span = None
    for event in stream:
        tkey = (event['usid'], event['tid'])
        yield event.copy() # yield a copy so we can work with the original event
        if event['msg'] == 'grabbing transaction lock':
            if event['type'] == 'start':
                grabbing_lock[tkey] = event
            elif event['type'] == 'stop':
                grabbing_lock.pop(tkey, None)

        if event['type'] == 'stop' and event['msg'] == 'grabbing transaction lock':
            if event.get('annotation', '') == 'error':
                # if grabbing transaction lock span was aborted or similar, we
                # can't assume this transaction got the transaction lock
                continue
            # start of holding transaction lock span
            hold_span = event.copy()
            hold_span['type'] = 'start'
            hold_span['span_id'] = secrets.token_hex(8)
            hold_span['parent'] = event.get('parent', '')
            hold_span['duration'] = None
            hold_span['msg'] = 'holding transaction lock'
            yield hold_span
            # emit virtual (fake) info message for other transactions waiting
            # on the transaction lock with info on that the lock holder has
            # changed
            for glkey, glevent in grabbing_lock.items():
                gl_span = glevent.copy()
                gl_span['type'] = 'info'
                gl_span['timestamp'] = event['timestamp']
                gl_span['msg'] = 'transaction lock holder changed'
                gl_span['struct'] = {'tlock_holder': hold_span['tid']}

        if event['type'] == 'info' and event['msg'] == 'releasing transaction lock':
            if hold_span is None:
                # weird, this shouldn't happen
                log.error(f"saw end of holding transaction lock [info/releasing transaction lock] but transaction-lock not held for transaction {event}")
            else:
                hold_stop = hold_span.copy()
                hold_stop['type'] = 'stop'
                hold_stop['duration'] = (event['timestamp']-hold_span['timestamp'])
                hold_stop['timestamp'] = event['timestamp']
                yield hold_stop
                hold_stop = None


def export_otel(tracer, stream):
    """Export to OTel collector

    This is what an ideal OTel export function looks like. If the NSO
    progress-trace output is shaped the right way, implementing the export
    function is as simple as this. Currently, the ptrace format does not look
    like this and we are reliant on the transmogrify function to reshape the
    events to emulate the ideal NSO ptrace format. It is likely desirable to
    move the functionality of the transmogrify function into NSO.
    """
    log = logging.getLogger('export-otel')
    traces = {}
    for event in stream:
        if event['trace_id'] not in traces:
            traces[event['trace_id']] = {
                'spans': {}
            }
        trace = traces[event['trace_id']]

        ignore_tags = ('type', 'trace_id', 'span_id', 'parent', 'timestamp', 'duration', 'struct')
        kvs = {k: v for k, v in event.items() if k not in ignore_tags and v != ''}
        # TODO: mark as error?
#        if 'annotation' in event and event['annotation'] != '':
#            kvs['error'] = True

        if event['type'] == 'start':
            if event['parent'] is None:
                parent_span_context = None
            else:
                parent_span_context = trace['spans'][event['parent']].get_span_context()

            span_context = trace_api.SpanContext(
                event['trace_id'].int,
                int(event['span_id'], 16),
                is_remote=False,
                trace_flags=trace_api.TraceFlags(trace_api.TraceFlags.SAMPLED)
            )
            span = trace_sdk._Span(
                name=event['msg'],
                context=span_context,
                parent=parent_span_context,
                sampler=tracer.sampler,
                resource=tracer.resource,
                attributes=kvs,
                span_processor=tracer.span_processor,
                kind=trace_api.SpanKind.INTERNAL,
                links=(),
                instrumentation_info=tracer.instrumentation_info,
                set_status_on_exception=True,
            )
            span.start(start_time=event['timestamp']*1000, parent_context=None)
            trace['spans'][event['span_id']] = span

        elif event['type'] == 'stop':
            try:
                span = trace['spans'][event['span_id']]
                span.end(event['timestamp']*1000)
            except KeyError:
                log.warning(f"Unable to find matching span for: {prettify(event)}")

        elif event['type'] == 'info':
            try:
                span = trace['spans'][event['span_id']]
                if span.end_time is not None:
                    log.warning(f"Adding event to an ended span. \n{prettify(event)} \n{span}")
                span.add_event(
                    event['msg'],
                    kvs,
                    event['timestamp']*1000
                )
            except KeyError:
                log.error(f"Unable to associate info event with an existing span - discarding {prettify(event)}")

        else:
            raise ValueError(f"Unhandled progress-trace event type {event['type']}")

        traces[event['trace_id']] = trace
        yield event


def export_influx_span(metrics_q, stream, extra_tags=None, conf=None):
    """Export span based metrics to InfluxDB
    """
    hostname = socket.gethostname()

    for event in stream:
        if event['type'] == 'stop':
            # Build default tags dict based on config, which is using a
            # DictSubscriber and thus can change between iterations. Also adding in
            # "host" tag for keeping track of which host exported this data.
            tags = {"host": hostname}
            if conf is not None:
                cfg_extra_tags = {data['name']: data['value'] for key, data in conf.extra_tags.items()}
                tags.update(cfg_extra_tags)
            if extra_tags is not None:
                tags.update(extra_tags)

            tags.update({"name": event['msg']})
            service_type = re.sub("\[[^\]]+\]", "", event.get('service', ''))
            tags.update({"service_type": service_type})

            fields = {
                "duration": float(event['duration'])/1000,
                "tid": event['tid'],
                "trace_id": str(event['trace_id']).replace('-', ''),
                "service": event.get('service', ''),
                "device": event.get('device', '')
            }
            metrics_q.append({
                "time": datetime.datetime.fromtimestamp(event['timestamp']/1000000),
                "measurement": "span",
                "tags": tags,
                "fields": fields
            })
        yield event


def export_influx_transaction(metrics_q, stream, extra_tags=None, conf=None):
    """Export metrics summarized per transaction to InfluxDB

    The duration of spans are summarized and exported at the end time of the
    transaction. Each span name becomes a field and its value is the summary of
    time of that span type during the transaction. For example, if there are
    two create spans each taking 2 seconds, the 'create' field for this
    transaction will be 4.
    """
    hostname = socket.gethostname()

    traces = {}
    for event in stream:
        tkey = (event['usid'], event['tid'])
        if event['type'] == 'stop':
            # Build default tags dict based on config, which is using a
            # DictSubscriber and thus can change between iterations. Also adding in
            # "host" tag for keeping track of which host exported this data.
            tags = {"host": hostname}
            if conf is not None:
                cfg_extra_tags = {data['name']: data['value'] for key, data in conf.extra_tags.items()}
                tags.update(cfg_extra_tags)
            if extra_tags is not None:
                tags.update(extra_tags)

            if tkey not in traces:
                traces[tkey] = {}

            if event['msg'] not in traces[tkey]:
                traces[tkey][event['msg']] = 0

            traces[tkey][event['msg']] += int(event['duration'])

            if event['msg'] == 'transaction':
                fields = traces[tkey]
                fields.update({'tid': event['tid']})
                metrics_q.append({
                    "time": datetime.datetime.fromtimestamp(event['timestamp']/1000000),
                    "measurement": "transaction",
                    "tags": tags,
                    "fields": fields
                })
        yield event


def export_influx_tlock(metrics_q, stream, extra_tags=None, conf=None):
    """Export transaction-lock related metrics to InfluxDB

    This tracks metrics associated with the transaction-lock itself;
    - transaction-lock held or not
    - transaction-lock queue-length
    """
    log = logging.getLogger('export_influx_tlock')
    hostname = socket.gethostname()

    held = 0
    waiting = {}
    tlock_qlen = 0

    # create database if it doesn't already exist
    for event in stream:
        tkey = (event['usid'], event['tid'])
        if event['msg'] in {'holding transaction lock', 'grabbing transaction lock'}:
            # Build default tags dict based on config, which is using a
            # DictSubscriber and thus can change between iterations. Also adding in
            # "host" tag for keeping track of which host exported this data.
            tags = {"host": hostname}
            if conf is not None:
                cfg_extra_tags = {data['name']: data['value'] for key, data in conf.extra_tags.items()}
                tags.update(cfg_extra_tags)
            if extra_tags is not None:
                tags.update(extra_tags)

            tags.update({"name": event['msg']})

            if event['msg'] == 'grabbing transaction lock':
                if event['type'] == 'start' and held == 1:
                    # if tlock is held, this transaction trying to grab the
                    # tlock will be waiting, so we add it to our list of
                    # waiting transactions and increase the queue length. Note
                    # how we don't increase the queue-length when the lock is
                    # not held, since the transaction asking for the lock will
                    # be able to get it immediately and thus won't have to wait
                    # - it is never in the queue.
                    waiting[tkey] = {'start': event['timestamp']}
                    tlock_qlen += 1
                if event['type'] == 'stop' and tkey in waiting:
                    # Only decrease for transactions we've identified are
                    # waiting, i.e. when we identified that our transactions
                    # was added to and increased the length of the queue.
                    # For example, when the lock is not held and we see the
                    # start of grabbing transaction lock, we don't increase the
                    # queue length, since we will immediately grab the lock.
                    # Then we must also not decrement
                    waiting.pop(tkey, None)
                    tlock_qlen -=1

                metrics_q.append({
                    "time": datetime.datetime.fromtimestamp(event['timestamp']/1000000.0),
                    "measurement": "transaction-lock",
                    "tags": tags,
                    "fields": {
                        "held": held,
                        "queue-length": tlock_qlen,
                        "tid": event['tid']
                    }
                })

            if event['msg'] == 'holding transaction lock':
                type_map = {'start': 1, 'stop': 0}
                held = type_map[event['type']]

                metrics_q.append({
                    "time": datetime.datetime.fromtimestamp(event['timestamp']/1000000.0),
                    "measurement": "transaction-lock",
                    "tags": tags,
                    "fields": {
                        "held": held,
                        "queue-length": tlock_qlen,
                        "tid": event['tid']
                    }
                })

        yield event

    if len(waiting) > 0:
        log.error("Unclean waiting transaction state at end:")
        log.error(waiting)


def sniffer(filename):
    """Takes a guess at the CSV version format of a file"""
    with open(filename) as csvfile:
        csvreader = csv.DictReader(csvfile)
        fields = csvreader.fieldnames

    if fields == ['EVENT TYPE', 'TRACE ID', 'SPAN ID', 'PARENT SPAN', 'TIMESTAMP', 'DURATION', 'SESSION ID', 'TRANSACTION ID', 'DATASTORE', 'CONTEXT', 'SUBSYSTEM', 'PHASE', 'SERVICE', 'SERVICE PHASE', 'COMMIT QUEUE ID', 'NODE', 'DEVICE', 'DEVICE PHASE', 'PACKAGE', 'MESSAGE', 'ANNOTATION', 'COMPONENT', 'STATE', 'STRUCTURED']:
        return "NG1", fields

    if fields == ['EVENT TYPE', 'TIMESTAMP', 'DURATION', 'SESSION ID', 'TRANSACTION ID', 'DATASTORE', 'CONTEXT', 'SUBSYSTEM', 'PHASE', 'SERVICE', 'SERVICE PHASE', 'COMMIT QUEUE ID', 'NODE', 'DEVICE', 'DEVICE PHASE', 'PACKAGE', 'MESSAGE', 'ANNOTATION']:
        return "5.4", fields

    if fields == ['TIMESTAMP', 'TID', 'SESSION ID', 'CONTEXT', 'SUBSYSTEM', 'PHASE', 'SERVICE', 'SERVICE PHASE', 'COMMIT QUEUE ID', 'NODE', 'DEVICE', 'DEVICE PHASE', 'PACKAGE', 'DURATION', 'MESSAGE']:
        return "5.3", fields

    return None, fields


def get_pipeline(stream):
    """Returns the standard pipeline

    This is just a convenience function. Perhaps it should be more automatic,
    doing the version sniff etc? But that requires lookahead on the stream,
    which we don't support.
    """
    return transmogrify(
        add_span_id(
            add_tlock_holder(
                fix_dev_snapshot(
                    fix_oper_commit_stop(
                        fix_ooo_write_start_stop(
                            fix_restconf_type(
                                fix_nano_events(
                                    fix_check_conflicts(
                                        stream)))))))))


def csv_ptrace_reader(filename):
    """Generator that yields progress-trace events as read from a
    progress-trace CSV file

    This will try to workaround deficiencies in older CSV exports where the
    type field isn't present. We try to recreate it based on the messages but
    this is somewhat error prone, at least theoretically, so don't be surprised
    if this breaks.
    """
    sniff_ver, fields = sniffer(filename)
    stream = csv_reader(filename)
    if sniff_ver == "5.3":
        stream = uplift_53(stream)
    if sniff_ver != 'NG1':
        stream = get_pipeline(stream)

    for row in stream:
        yield row


class InfluxdbExporter(threading.Thread):
    """Exports metrics from our queue to InfluxDB

    Runs as a separate thread to avoid blocking the main thread with the
    otherwise synchronous export to InfluxDB. This could potentially be
    replaced by an async library for InfluxDB export.
    """
    def __init__(self, influx_client, metrics_q):
        super().__init__()
        self.client = influx_client
        self.q = metrics_q
        self.exit_flag = False
        self.log = logging.getLogger("influxdb-exporter")
        try:
            self.client.create_database("nso")
        except:
            pass


    def run(self):
        while not self.exit_flag:
            sleep = self.export()
            if sleep:
                time.sleep(0.1)

    def export(self, batch=500):
        # InfluxDB page states optimal batch size for line protocol is 5000.
        # What API is the influxdb client library using? We use a more
        # conservative batch size of 500 here.
        dps = []
        res = False
        i = 0
        while True:
            try:
                dps.append(self.q.pop())
            except IndexError:
                # If we hit end of queue, return true so we can sleep a bit
                # before next export run
                res = True
                break
            if batch is not None and i > batch:
                break
            i += 1
        if len(dps) > 0:
            self.client.write_points(dps)
        return res


    def stop(self):
        # Tell thread to exit
        self.exit_flag = True
        self.log.info(f"Shutting down. {len(self.q)} still in queue, exporting in batches...")
        # Export remaining things on queue
        while True:
            eoq = self.export()
            if eoq:
                self.log.info("Metrics queue emptied. Exiting.")
                break


def retime(stream, start):
    """Retime progress-trace to start right now

    Rewrites the timestamps of events. The first events timestamp is changed to
    right now and then all subsequent events timestamps are rewritten so that
    their relative time to the first event is maintained.
    """
    orig_start_ts = None
    cal = parsedatetime.Calendar()
    new_start, _ = cal.parseDT(start)
    new_start_ts = int(new_start.strftime("%s%f"))
    for event in stream:
        if orig_start_ts is None:
            # store original event, before rewriting the timestamp so we can
            # compute the delta for subsequent events
            orig_start_ts = event['timestamp']

        delta = int(event['timestamp'] - orig_start_ts)
        new = event.copy()
        new['timestamp'] = new_start_ts + delta

        yield new


def rando(stream):
    """Reassign usid & tid to random data

    This is useful for creating mock data without having to manually work with
    the data.

    This might be rather memory intensive on large trace files as the state is
    never cleaned up. The envisaged use case is mostly focused on repeating
    smaller trace files though, so this should not be much of an actual issue.
    """
    rewrite_usid = {}
    rewrite_tid = {}
    last_usid = random.randint(0,9999)
    last_tid = random.randint(0,9999)
    for original_event in stream:
        event = original_event.copy()

        if event['usid'] not in rewrite_usid:
            rewrite_usid[event['usid']] = random.randint(last_usid, last_usid + 100)
            last_usid = rewrite_usid[event['usid']]
        event['usid'] = rewrite_usid[event['usid']]

        if event['tid'] not in rewrite_tid:
            rewrite_tid[event['tid']] = random.randint(last_tid, last_tid + 100)
            last_tid = rewrite_tid[event['tid']]
        event['tid'] = rewrite_tid[event['tid']]

        yield event


def progress_printer(stream, csvfile):
    """Display progress bar of processing CSV file

    Since we are acting on stream, which is a generator, we don't know what the
    total amount of entries is. Thus, we peek into the CSV file to first get
    the total number of lines. This isn't entirely exact as other stages in the
    processing pipeline might add more messages, thus yielding a higher total
    number of events than in the CSV file.
    """
    # First count number of lines in CSV file - this should be fairly quick
    from rich.progress import Progress
    with Progress() as progress:
        task1 = progress.add_task("[green]Processing CSV...", start=False)
        lines = 0
        with open(csvfile, 'rt') as file:
            for i, l in enumerate(file):
                lines = i+1
        progress.update(task1, total=lines)
        progress.start_task(task1)
        for event in stream:
            progress.update(task1, advance=1)


def memprofiler(stream):
    from guppy import hpy
    h = hpy()
    i = 0
    for event in stream:
        i += 1
        if i > 100:
            i = 0
            print(h.heap().byrcs)



if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--log-file')
    parser.add_argument('--export-jaeger', default=False, action='store_true')
    parser.add_argument('--export-influxdb', default=False, action='store_true')
    parser.add_argument('--jaeger-host', default='localhost')
    parser.add_argument('--jaeger-port', type=int, default=6831)
    parser.add_argument('--influxdb-host')
    parser.add_argument('--influxdb-port', type=int, default=8086)
    parser.add_argument('--influxdb-db', default='nso')
    parser.add_argument('--influxdb-username')
    parser.add_argument('--influxdb-password')
    parser.add_argument('--influxdb-drop', action="store_true")
    parser.add_argument('--csv')
    parser.add_argument('--csv-out')
    parser.add_argument('--extra-tags', action="append", default=[])
    parser.add_argument('--retime')
    parser.add_argument('--rando', action="store_true")
    parser.add_argument('--memprofile', action="store_true")
    args = parser.parse_args()

    log_level = logging.INFO
    if args.debug:
        log_level = logging.DEBUG
    from rich.logging import RichHandler
    log_handlers = [RichHandler()]

    if args.log_file:
        print("Adding file log handler")
        log_handlers.append(logging.FileHandler(args.log_file))

    logging.basicConfig(
        level=log_level,
        format="%(message)s",
        datefmt="[%X]",
        handlers=log_handlers
    )

    extra_tags = {}
    for et in args.extra_tags:
        extra_tags[et.split("=")[0]] = et.split("=")[1]

    if args.csv:
        jaeger_exporter = jaeger.JaegerSpanExporter(
            service_name="NSO",
            agent_host_name=args.jaeger_host,
            agent_port=args.jaeger_port,
        )

        trace_api.set_tracer_provider(TracerProvider())
        trace_api.get_tracer_provider().add_span_processor(
            SimpleExportSpanProcessor(jaeger_exporter)
        )
        tracer = trace_api.get_tracer('opentelemetry-exporter')
        influx_client = InfluxDBClient(args.influxdb_host, args.influxdb_port, args.influxdb_username, args.influxdb_password, args.influxdb_db)

        if args.influxdb_drop:
            influx_client.drop_database("nso")

        stream = csv_ptrace_reader(args.csv)
        if args.retime:
            stream = retime(stream, args.retime)
        if args.rando:
            stream = rando(stream)
        influx_exporter = None
        if args.export_jaeger:
            stream = export_otel(tracer, stream)
        if args.export_influxdb:
            metrics_q = []
            influx_exporter = InfluxdbExporter(influx_client, metrics_q)
            influx_exporter.start()
            stream = export_influx_span(metrics_q, stream, extra_tags=args.extra_tags)
            stream = export_influx_tlock(metrics_q, stream, extra_tags=args.extra_tags)
            stream = export_influx_transaction(metrics_q, stream, extra_tags=args.extra_tags)
        try:
            if args.csv_out:
                csv_writer(args.csv_out, stream)
            elif args.memprofile:
                memprofiler(stream)
            else:
                progress_printer(stream, args.csv)
        except KeyboardInterrupt:
            pass
        if influx_exporter is not None:
            influx_exporter.stop()
            influx_exporter.join()

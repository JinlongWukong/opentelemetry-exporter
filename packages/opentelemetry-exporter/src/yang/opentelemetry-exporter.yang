module opentelemetry-exporter {
  yang-version "1.1";

  namespace "http://tail-f.com/ns/opentelemetry-exporter";
  prefix opentelemetry-exporter;

  import ietf-inet-types {
    prefix inet;
  }
  import tailf-common {
    prefix tailf;
  }
  import tailf-ncs {
    prefix ncs;
  }
  import tailf-progress {
    prefix progress;
  }

  description
    "Configuration of the OpenTelemetry exporter";

  revision 2020-05-03 {
    description
      "Initial revision";
  }

  augment "/progress:progress" {
    description "Augment in ptrace export configuration";

    container export {
      description "Export progress-trace to external systems";

      leaf enabled {
        type boolean;
        default true;
        description "Enable the export processor";
      }

      action restart {
        tailf:actionpoint "ptrace-export-restart";
        output {
          leaf result {
            type string;
            description "Result";
          }
        }
      }

      container influxdb {
        presence influxdb;
        description "Export metrics derived from progress-trace to InfluxDB";

        leaf host {
          type inet:host;
          mandatory true;
          description "InfluxDB host to export metrics to";
        }

        leaf port {
          type inet:port-number;
          default 8086;
          description "InfluxDB port to export metrics to";
        }

        leaf username {
          type string;
          description "InfluxDB username";
        }

        leaf password {
          type tailf:aes-256-cfb-128-encrypted-string;
          description "InfluxDB password";
        }

        leaf database {
          type string;
          default "nso";
          description "InfluxDB database name";
        }

      }

      container jaeger {
        presence jaeger;
        description "Export trace information to a Jaeger system";

        leaf host {
          type inet:host;
          default "localhost";
          description "Jaeger host to send trace information to";
        }

        leaf port {
          type inet:port-number;
          default 6831;
          description "Port on Jaeger agent to send trace information to";
        }
      }

      leaf logging {
        type boolean;
        default false;
        description "Enable Python logging of tracing";
      }

      list extra-tags {
        key name;
        description "Extra tags to add to every exported span";

        leaf name {
          type string;
          description "Name (key) of the tag";
        }

        leaf value {
          type string;
          description "Value of the tag";
        }
      }

      leaf jaeger-base-url {
        type string;
        description "Base URL to the Jaeger query system. This is used to form the URL in the transaction annotation link.";
      }

    }
  }
}
